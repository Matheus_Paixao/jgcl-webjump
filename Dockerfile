FROM php:cli-alpine3.9

RUN apk --update add --no-cache \
    ${PHPIZE_DEPS} \
    openssl-dev \
    git \
    libxml2-dev \
    && rm -rf /var/cache/apk/*

RUN docker-php-ext-install \
        pdo_mysql \
        opcache

RUN chmod -R 755 /usr/local/lib/php/extensions/ \
    && curl -sS https://getcomposer.org/installer | php -- --install-dir=/usr/bin --filename=composer \
    && mkdir -p /app \
    && chown -R www-data:www-data /app

# configs files
COPY ./app /app
COPY ./docker/start.sh /start.sh

# workdir
WORKDIR /app

# laravel install, chmod, others
RUN chmod +x /start.sh \
    && composer install \
    && composer dump-autoload -a \
    && php artisan optimize

# entrypoint
ENTRYPOINT ["sh", "-c"]

# start
CMD ["/start.sh"]
