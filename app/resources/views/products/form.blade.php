@extends('layout')

@section('content')
    <!-- Main Content -->
    <main class="content">
        <h1 class="title new-item">New Product</h1>


        <div class="error"><ul>
        @foreach($errors->all() as $message)
            <li>{{ $message }}</li>
        @endforeach
        </div></ul>

        <form action="{{ $url }}" method="POST">
            <input type="hidden" name="_token" value="{{ csrf_token() }}">
            <input type="hidden" name="id" value="{{ $product->id ?? "" }}">

            <div class="input-field">
                <label for="sku" class="label">Product SKU</label>
                <input type="text" name="sku" id="sku" class="input-text" value="{{ old('sku') ?? $product->sku }}" />
            </div>
            <div class="input-field">
                <label for="name" class="label">Product Name</label>
                <input type="text" name="name" id="name" class="input-text" value="{{ old('name') ?? $product->name }}" />
            </div>
            <div class="input-field">
                <label for="price" class="label">Price</label>
                <input type="text" name="price" id="price" class="input-text" value="{{ old('price') ?? $product->price }}" />
            </div>
            <div class="input-field">
                <label for="quantity" class="label">Quantity</label>
                <input type="text" name="quantity" id="quantity" class="input-text" value="{{ old('quantity') ?? $product->quantity }}" />
            </div>
            <div class="input-field">
                <label for="category" class="label">Categories</label>
                <select multiple name="category[]" id="category" class="input-text">
                    @foreach($categories as $category)
                        <option value="{{ $category->id }}" {{ in_array($category->id, (old('category') ?? $product->categories->pluck('id')->toArray())) ? 'selected' : ''}}>{{ $category->name }}</option>
                    @endforeach
                </select>
            </div>
            <div class="input-field">
                <label for="description" class="label">Description</label>
                <textarea id="description" name="description" class="input-text">{{ old('description') ?? $product->description }}</textarea>
            </div>
            <div class="actions-form">
                <a href="{{ route("products.index") }}" class="action back">Back</a>
                <input class="btn-submit btn-action" type="submit" value="Save Product" />
            </div>

        </form>
    </main>
    <!-- Main Content -->
@endsection


