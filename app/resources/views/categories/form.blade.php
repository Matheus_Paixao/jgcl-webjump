@extends('layout')

@section('content')
    <!-- Main Content -->
    <main class="content">
        <h1 class="title new-item">New Category</h1>


        <div class="error"><ul>
        @foreach($errors->all() as $message)
            <li>{{ $message }}</li>
        @endforeach
        </div></ul>

        <form action="{{ $url }}" method="POST">
            <input type="hidden" name="_token" value="{{ csrf_token() }}">
            <input type="hidden" name="id" value="{{ $category->id ?? "" }}">

            <div class="input-field">
                <label for="sku" class="label">Category Name</label>
                <input type="text" name="name" id="name" class="input-text" value="{{ old('name') ?? $category->name }}" />
            </div>
            <div class="input-field">
                <label for="name" class="label">Category Code</label>
                <input type="text" name="code" id="code" class="input-text" value="{{ old('code') ?? $category->code }}" />
            </div>
            <div class="actions-form">
                <a href="{{ route("categories.index") }}" class="action back">Back</a>
                <input class="btn-submit btn-action" type="submit" value="Save Category" />
            </div>
        </form>
    </main>
    <!-- Main Content -->
@endsection


