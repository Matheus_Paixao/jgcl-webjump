<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/test', 'PhpDebugController@test');

Route::get('/', 'DashBoardController@index')->name('dashboard.index');

Route::get('/products', 'ProductController@index')->name('products.index');
Route::get('/products/create', 'ProductController@create')->name('products.create');
Route::post('/products', 'ProductController@store')->name('products.store');
Route::get('/products/{product}/edit', 'ProductController@edit')->name('products.edit');
Route::post('/products/{product}', 'ProductController@update')->name('products.update');
Route::get('/products/{product}/delete', 'ProductController@destroy')->name('products.destroy');

Route::get('/categories', 'CategoryController@index')->name('categories.index');
Route::get('/categories/create', 'CategoryController@create')->name('categories.create');
Route::post('/categories', 'CategoryController@store')->name('categories.store');
Route::get('/categories/{product}/edit', 'CategoryController@edit')->name('categories.edit');
Route::post('/categories/{product}', 'CategoryController@update')->name('categories.update');
Route::get('/categories/{product}/delete', 'CategoryController@destroy')->name('categories.destroy');

