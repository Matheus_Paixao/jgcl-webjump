<?php

/**
* @SWG\Swagger(
*     basePath="/api/v1",
*     schemes={"http"},
*     host=L5_SWAGGER_CONST_HOST,
*     @SWG\Info(
*         version="1.0.0",
*         title="L5 Swagger API",
*         description="L5 Swagger API description",
*         @SWG\Contact(
*             email="darius@matulionis.lt"
*         ),
*     )
* )
*/