<?php

namespace App\Http\Controllers;

use App\Services\ProductService;
use Illuminate\Http\Request;

/**
 * Class DashBoardController
 * @package App\Http\Controllers
 */
class DashBoardController extends Controller
{
    /** @var ProductService */
    private $productService;

    /**
     * ProductController constructor.
     */
    public function __construct()
    {
        $this->productService = new ProductService();
    }

    /**
     * @param Request $request
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function index(Request $request)
    {
        $products = $this->productService->getPaginate($request->input('per_page', 4));

        return view('dashboard', ['products' => $products]);
    }
}
