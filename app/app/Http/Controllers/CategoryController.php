<?php

namespace App\Http\Controllers;

use App\Models\CategoryModel;
use App\Services\CategoryService;
use Illuminate\Http\Request;

class CategoryController extends Controller
{
    /** @var CategoryService */
    private $categoryService;

    /**
     * constructor.
     */
    public function __construct()
    {
        $this->categoryService = new CategoryService();
    }

    /**
     * Display a listing of the resource.
     *
     * @param Request $request
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function index(Request $request)
    {
        $categories = $this->categoryService->getPaginate($request->input('per_page', 10));
        return view('categories.index', ['categories' => $categories]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
        return view('categories.form', [
            'category' => new CategoryModel(),
            'url' => route("categories.store")
        ]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $data = $request->only(['name', 'code']);

        $this->categoryService->validade($data);

        $category = $this->categoryService->create($data);

        return redirect()->route('categories.edit', ['id'=>$category->id]);

    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
        $category = $this->categoryService->getById($id);
        return view('categories.form', [
            'category' => $category,
            'url' => route("categories.update", ['id' => $category->id])
        ]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $data = $request->only(['code', 'name']);
        $data['id'] = $id;

        $this->categoryService->validade($data);

        $category = $this->categoryService->edit($data);

        return redirect()->route('categories.edit', ['id'=>$category->id]);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
        $this->categoryService->delete($id);

        return redirect()->route('categories.index');
    }
}
