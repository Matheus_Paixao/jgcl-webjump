<?php

namespace App\Http\Controllers;

use App\Models\CategoryModel;
use App\Models\ProductModel;
use Illuminate\Http\Request;

/**
 * Class PhpDebugController
 * @package App\Http\Controllers
 */
class PhpDebugController extends Controller
{
    /**
     * @param Request $request
     * @return mixed
     */
    public function test(Request $request)
    {
        return '2';
    }
}
