<?php

namespace App\Console\Commands;

use App\Services\CsvProductService;
use Illuminate\Console\Command;

/**
 * Class ImportProductsCsvCommand
 * @package App\Console\Commands
 */
class ImportProductsCsvCommand extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'webjump:importcsv {file?}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'webjump:importcsv {file?}';

    /**
     * The CSV Product Service
     *
     * @var \App\Services\CsvProductService
     */
    protected $csvProductService;

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct(CsvProductService $csvProductService)
    {
        parent::__construct();
        $this->csvProductService = $csvProductService;
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $filePath = $this->getFilePath($this->argument('file'));

        $this->csvProductService->saveProductsFromCsv($filePath);
    }

    /**
     * @param null|string $file
     * @return string
     */
    private function getFilePath(?string $file): string
    {
        if(is_file(storage_path('app/'.$file))) {
            return storage_path('app/'.$file);
        } else {
            return storage_path('app/import.csv');
        }
    }
}
