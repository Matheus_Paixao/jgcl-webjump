<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Relations\BelongsToMany;

/**
 * Class CategoryModel
 * @package App\Models
 */
class ProductModel extends \Illuminate\Database\Eloquent\Model
{
    protected $table = 'products';
    public $timestamps = true;
    protected $dates = ['created_at', 'updated_at'];
    protected $fillable = [
        'name',
        'sku',
        'description',
        'quantity',
        'price',
    ];
    protected $casts = [
        'name' => "string",
        'sku' => "string",
        'description' => "string",
        'quantity' => "string",
        'price' => "float",
    ];

    /**
     * @return BelongsToMany
     */
    public function categories(): BelongsToMany
    {
        return $this->belongsToMany(CategoryModel::class, 'category_product', 'product_id', 'category_id');
    }

}